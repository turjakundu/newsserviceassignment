package com.javacode.enterprise.rest.jersey;

import java.io.File;
import java.util.*;
import java.util.concurrent.*;

import javax.xml.bind.Binder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

class RssThread  {
	
	 private String filePath;
	 Logger logger;
	 public  String logDirectory = "//var//News//Logs//";
	 public  String rssfile = "//var//News//rssfeed.xml";
	 public RssThread(){}
	 public RssThread(String filePath)
	 {
		 this.filePath = filePath;
		 this.logger = new Logger(logDirectory); 
		 
	 }
	
	 public static class getSingleNodeCallable implements Callable<NewsItem> {
		 
	 private NewsItem newsItem;
	 private String file;
	 JAXBContext jaxbContext;
	 public  String logDirectory = "//var//News//Logs//";
	 Logger logger;
	 

	 
	 
	 public getSingleNodeCallable(String file,JAXBContext jaxbContext) {
	   this.file = file;
	   this.jaxbContext = jaxbContext;
	   this.logger = new Logger(logDirectory);;
	 }
	 @Override
	 public  NewsItem call() {
			try {
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(); 
				DocumentBuilder db = dbf.newDocumentBuilder();
				System.out.println(file);
				File parseFile= new File(file);
				
				Document document = db.parse(parseFile);
			    JAXBContext jaxbContext = JAXBContext.newInstance(NewsList.class);
			    Binder<Node> binder = jaxbContext.createBinder();
			    NewsList newsList = (NewsList) binder.unmarshal(document);
			    int size = newsList.getNews().size();
			    newsItem = new NewsItem(newsList.getNews().get(size-1).getTitle(), newsList.getNews().get(size-1).getBody(), newsList.getNews().get(size-1).getAuthor(), newsList.getNews().get(size-1).getDate());
			    		
			
				} catch (Exception e) {
					e.printStackTrace();
				    logger.writeError("nnThreadedRSS"+"::"+e.toString() );
				}
			
			return newsItem;

		    
		 }
	}

public  void rssGenerate() {
	 ArrayList<RSSEntry> entries = new ArrayList<RSSEntry>();
	 RSSFeed feed = new RSSFeed(); 
	 ExecutorService pool = Executors.newFixedThreadPool(10);
	 final ConcurrentMap<String, Object> locks = new ConcurrentHashMap<>();

	 Set<Future<NewsItem>> set = new HashSet<Future <NewsItem>>();
	 
	 
	 final File folder = new File(filePath);
	 JAXBContext jaxbContext = null;
     RSSHeader header = new RSSHeader();
     header.setCopyright("Copyright by Author");
     header.setTitle("Sample Rss Feed");
     header.setDescription("Rss feed for news data");
     header.setLanguage("en");
     header.setLink("http://www.cefalo.com");
     header.setPubDate(RSSFeed.formatDate(Calendar.getInstance()));
     feed.setHeader(header);

	 try {
		jaxbContext = JAXBContext.newInstance(NewsList.class);
	 } catch (JAXBException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		logger.writeError("nnThreadedRSS"+"::"+e.toString() );
	 }
	 //for (String word: args) {
	 for ( final File fileEntry : folder.listFiles()) {	 
		 String filename = filePath+fileEntry.getName().replaceAll("/", "//");
		 System.out.println(filename);
		 File file = new File(filename);				
		 if(file.exists() && !file.isDirectory()) 
		 { 
				try {
					
					   Callable<NewsItem> callable = new getSingleNodeCallable(filename,jaxbContext);
					   Future<NewsItem> future = pool.submit(callable);
					   set.add(future);
//				    entry.setTitle(newsList.getNews().get(newsList.getNews().size() -1).getTitle());
//	                entry.setTitle(newsList.getNews().get(newsList.getNews().size() -1).getBody());
//		
					} catch (Exception e) {
						e.printStackTrace();
					    logger.writeError("nnThreadedRSS"+"::"+e.toString() );
					}
		 }

	 }
	 
	 int sum = 0;
	 
	 pool.shutdown();
	 try {
		 pool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
	 } catch (InterruptedException e) {
		 logger.writeError("nnThreadedRSS"+"::"+e.toString() );	
	 }
	 for (Future<NewsItem> future : set) {
		 
		 
		 NewsItem newsItem =null;
		 try {
			newsItem = future.get();
		 } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.writeError("nnThreadedRSS"+"::"+e.toString() );
			
		 } catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.writeError("nnThreadedRSS"+"::"+e.toString() );			
		 }
		
		 RSSEntry entry = new RSSEntry();
		 entry.setTitle(newsItem.getTitle());
	     entry.setDescription(newsItem.getBody());
         entry.setGuid("http://bbc.com/index.htm");
         entry.setLink("http://cnn.com/rgagnon/download/index.htm");
         entry.setPubDate(RSSFeed.formatDate(Calendar.getInstance()));
         entries.add(entry);
		 

		
		    
	 }

     feed.setEntries(entries);
     
     try {
     	 RSSWriter.write(feed, rssfile);
     } 
     catch (Exception e) {
       e.printStackTrace();
     }
     System.out.println("Done.");
	 
	 
	 

     
     
     
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 

	}

}