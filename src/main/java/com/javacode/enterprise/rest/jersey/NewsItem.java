package com.javacode.enterprise.rest.jersey;



import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.*;



@XmlRootElement(name = "news")
@XmlAccessorType (XmlAccessType.FIELD)
public class NewsItem {

   private String title;
   private String body;
   private String author;
   private String date;
//   private String date;

   public String getTitle() {
      return this.title;
   }

   public void setTitle(String value) {

      this.title = value.replaceAll("\n", "");;
   }	 
   public String getBody() {
      return this.body;
   }

   public void setBody(String value) {
      this.body = value.replaceAll("\n", "");;
   }
   public String getAuthor() {
	      return this.author;
   }

   public void setAuthor(String value) {
      this.author = value.replaceAll("\n", "");;
   }
   
   public String getDate() {
	  return this.date;
   }
	
   public void setDate(String value) {
      this.date = value.replaceAll("\n", "");;
   }

	// Must have no-argument constructor
	public NewsItem() {
	
	}

	public NewsItem(String title, String body,String author,String date) {
		this.title = title;
		this.body = body;
		this.author = author;
		this.date = date;
	
	}
	@Override
	public String toString() {
		return "news [title=" + title + ", body=" + body +", author=" + author + "]";
	}


}