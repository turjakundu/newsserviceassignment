package com.javacode.enterprise.rest.jersey;



import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;




@XmlRootElement(name = "news")
public class News {

   private String title;
   private String body;
   private String author;
//   private String date;


   //private OperatorRepository operatorRepository;
   public String getTitle() {
      return this.title;
   }
   @XmlElement
   public void setTitle(String value) {

      this.title = value.replaceAll("\n", "");;
   }	 
   public String getBody() {
      return this.body;
   }
   @XmlElement
   public void setBody(String value) {
      this.body = value.replaceAll("\n", "");;
   }
   public String getAuthor() {
	      return this.author;
   }
//   @XmlElement
//   public void setDate(String value) {
//      this.date = value.replaceAll("\n", "");;
//   }
//   public String getDate() {
//	      return this.date;
//   }
   @XmlElement
   public void setAuthor(String value) {
      this.author = value.replaceAll("\n", "");;
   }

	// Must have no-argument constructor
	public News() {
	
	}

	public News(String title, String body,String author) {
		this.title = title;
		this.body = body;
		this.author = author;
	
	}
	
//	public login getDialerInfo()
//	{
//		operatorRepository = new OperatorRepository();
//	//		 String logDirectory = "//root//Logs//";	
//	//		 MyLogger logger;
//	//		 logger = new MyLogger(logDirectory);
//	//		 
//	//		 logger.writeError("getDialerInfo" );
//		return operatorRepository.OperatorInfo(brandPin, userName);
//	}
	@Override
	public String toString() {
		return "news [title=" + title + ", body=" + body+ ", author="+author+ "]";
	}

}