package com.javacode.enterprise.rest.jersey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.*;


public class Logger {
	private String logDir;	
	private BufferedWriter logFile;
		
	public Logger(String ld)
	{
		logDir = ld;
		
		if(!logDir.endsWith("//") && !logDir.endsWith("//"))
			logDir += "//";		
	}
	
	private synchronized void write(String logType, String info)
	{
		String filename = logDir + logType + "//" + new SimpleDateFormat("yyyy_MM_dd").format(new Date())+".txt";
		try
		{
			logFile = new BufferedWriter(new FileWriter(filename, true));		
			logFile.write(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " | " + info);
			logFile.newLine();
			logFile.close();			
		}
		catch(Exception ex)
		{
			System.out.println("Log writer exception: " + ex.getMessage());
			ex.printStackTrace();
		}
	}
	
	public void writeInfo(String info)
	{		
		write("Info", info);
	}
	
	public void writeError(String info)
	{
		write("Error", info);
		write("Info", info);
	}
	
	public void writeReceived(String info)
	{
		write("Received", info);		
	}
	public void writeSent(String info)
	{
		write("Sent", info);
	}
}
