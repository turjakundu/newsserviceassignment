package com.javacode.enterprise.rest.jersey;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.Binder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.io.File;

import javax.xml.bind.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;

//import org.apache.struts2.ServletActionContext;

@Path("/xmlServices")
public class JerseyRestService {


//http://localhost:8080/NewsServiceAssignment/rest/xmlServices/print/turja	

    public  String logDirectory = "//var//News//Logs//";
    public  String datapath = "//var//News//data//";
    public  String rssfile = "//var//News//rssfeed.xml";
    
	
	private  String listFilesForFolder(final File folder,String title) {
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            
	        } else {
	            if(fileEntry.getName().contains(title))
	            {
	            	return fileEntry.getName().toString();
	            }
	        }
	    }
	    
	    return null;
	}

	
    private void generate() {
 
		Logger logger = new Logger(logDirectory);;
        RSSFeed feed = new RSSFeed();       
        RSSHeader header = new RSSHeader();
        header.setCopyright("Copyright by Author");
        header.setTitle("Sample Rss Feed");
        header.setDescription("Rss feed for news data");
        header.setLanguage("en");
        header.setLink("http://www.cefalo.com");
        header.setPubDate(RSSFeed.formatDate(Calendar.getInstance()));

        feed.setHeader(header);
        
        ArrayList<RSSEntry> entries = new ArrayList<RSSEntry>();

        final File folder = new File(datapath);
        
        for ( final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            
	        } else {
	        	try {
	        			        		
		                RSSEntry entry = new RSSEntry();	        		
		        		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(); 
		        		DocumentBuilder db = dbf.newDocumentBuilder();
		        	    String filename = datapath+fileEntry.getName().replaceAll("/", "//");
		        	    System.out.println(filename);
						File file = new File(filename);				
						if(file.exists() && !file.isDirectory()) 
						{ 
						    // do something
							try {
								Document document = db.parse(file);
							    JAXBContext jaxbContext = JAXBContext.newInstance(NewsList.class);
							    Binder<Node> binder = jaxbContext.createBinder();
							    NewsList newsList = (NewsList) binder.unmarshal(document);
							    entry.setTitle(newsList.getNews().get(newsList.getNews().size() -1).getTitle());
				                entry.setTitle(newsList.getNews().get(newsList.getNews().size() -1).getBody());
					
								} catch (Exception e) {
									e.printStackTrace();
								    logger.writeError("nnJsonResponse"+"::"+e.toString() );
								}
						}
						                
	        						              
		                entry.setGuid("http://bbc.com/index.htm");
		                entry.setLink("http://cnn.com/rgagnon/download/index.htm");
		                entry.setPubDate(RSSFeed.formatDate(Calendar.getInstance()));
		                entries.add(entry);
			
				} catch (Exception e) {
				e.printStackTrace();
				logger.writeError("nnRssCreation"+"::"+e.toString() );
				}
	        }
	    }
        
        feed.setEntries(entries);
        
        try {
        	 RSSWriter.write(feed, rssfile);
        } 
        catch (Exception e) {
          e.printStackTrace();
        }
        System.out.println("Done.");

        
    }
  //http://localhost:8080/NewsServiceAssignment/rest/xmlServices/feed	
    @GET
    @Path("/feed")
    @Produces("application/rss+xml")
    public Response getFeed()  {
    	
    	StringBuilder sb = new StringBuilder();
		Logger logger = new Logger(logDirectory);;
		BufferedReader br = null;
        // Write the SyndFeed to a Writer.
    	try
    	{
	        generate();
	        br = new BufferedReader(new FileReader(new File(rssfile)));
	        String line;
	        
	
	        while((line=br.readLine())!= null){
	            sb.append(line.trim());
	        }
	        System.out.println(sb.toString());
	       br.close();
    	}catch(Exception e)
    	{
    		e.printStackTrace();
		    logger.writeError("nnRssFeedResponse"+"::"+e.toString() );
    	}

        // Return a JAX-RS Response with the Writer as the body.
        return Response.ok(sb.toString()).build();
    }

    
    //http://localhost:8080/NewsServiceAssignment/rest/xmlServices/feedThrd	
    @GET
    @Path("/feedThrd")
    @Produces("application/rss+xml")
    public Response getFeedThrd()  {
    	
    	StringBuilder sb = new StringBuilder();
		Logger logger = new Logger(logDirectory);;
		BufferedReader br = null;
        // Write the SyndFeed to a Writer.
    	try
    	{
    		RssThread rssThread = new RssThread(datapath);
    		rssThread.rssGenerate();
	        br = new BufferedReader(new FileReader(new File(rssfile)));
	        String line;
	        
	
	        while((line=br.readLine())!= null){
	            sb.append(line.trim());
	        }
	        System.out.println(sb.toString());
	       br.close();
    	}catch(Exception e)
    	{
    		e.printStackTrace();
		    logger.writeError("nnRssFeedResponse"+"::"+e.toString() );
    	}

        // Return a JAX-RS Response with the Writer as the body.
        return Response.ok(sb.toString()).build();
    }
  //http://localhost:8080/NewsServiceAssignment/rest/xmlServices/json/assaignmnet		
	
	@GET
	@Path("/json/{title}")
	@Produces(MediaType.APPLICATION_JSON)
	public NewsItem getNewsInJSON(@PathParam("title") String title) {

		Logger logger = new Logger(logDirectory);;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(); 
        final File folder = new File(datapath);
		String fileName  = datapath+ listFilesForFolder(folder,title);
		
		  try {
				DocumentBuilder db = dbf.newDocumentBuilder();
				File file = new File(fileName);				
				if(file.exists() && !file.isDirectory()) 
				{ 
				    // do something
					try {
						Document document = db.parse(file);
					    JAXBContext jaxbContext = JAXBContext.newInstance(NewsList.class);
					    Binder<Node> binder = jaxbContext.createBinder();
					    NewsList newsList = (NewsList) binder.unmarshal(document);
					    return newsList.getNews().get(newsList.getNews().size()-1);
					
						} catch (Exception e) {
							e.printStackTrace();
						    logger.writeError("nnJsonResponse"+"::"+e.toString() );
						}
				}
		  }	 
	      catch (Exception e) {
			e.printStackTrace();
			logger.writeError("nnJsonResponse"+"::"+e.toString() );
		  }

		return null;

	}

	//http://localhost:8080/NewsServiceAssignment/rest/xmlServices/text/assaignment	
	@GET
	@Path("/text/{title}")
	@Produces(MediaType.TEXT_PLAIN)
	public String responseMsg( @PathParam("title") String title ) {
		
		 final File folder = new File(datapath);
		 Logger logger = new Logger(logDirectory);
         DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();       
		 String fileName  = datapath+ listFilesForFolder(folder,title);
		
	      try {
				DocumentBuilder db = dbf.newDocumentBuilder();
				File file = new File(fileName);	
				if(file.exists() && !file.isDirectory()) 
				{ 
					try {
						Document document = db.parse(file);
					    JAXBContext jaxbContext = JAXBContext.newInstance(NewsList.class);
					    Binder<Node> binder = jaxbContext.createBinder();
					    NewsList newsList = (NewsList) binder.unmarshal(document);
					    return newsList.getStory();
				
					} catch (Exception e) {
					e.printStackTrace();
					logger.writeError("nnTextResponse"+"::"+e.toString() );
					}
				}
		      }	 
	      	catch (Exception e) {
			e.printStackTrace();
			logger.writeError("nnTextResponse"+"::"+e.toString() );
		  }
	      return "";
	        
	}
	
	
	
	
	

	
	@POST
	@Path("/update")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response consumeXML(
			News news			
			) {
		

		 Logger logger = new Logger(logDirectory);
		 Calendar cal = Calendar.getInstance();
	     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
         DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        
	      try {
	    	  
	    	DocumentBuilder db = dbf.newDocumentBuilder();
	        File file = new File(datapath+news.getTitle()+news.getAuthor()+".xml");

	        if(file.exists() && !file.isDirectory()) 
	        { 

				try {
				    	Document document = db.parse(file);
				        JAXBContext jaxbContext = JAXBContext.newInstance(NewsList.class);
				        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
				        Binder<Node> binder = jaxbContext.createBinder();
				        NewsList newsList = (NewsList) binder.unmarshal(document);
				        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				        NewsItem newsItem= new NewsItem(news.getTitle(),news.getBody(),news.getAuthor(),sdf.format(cal.getTime()));			        
				        newsList.addNews(newsItem);
				        jaxbMarshaller.marshal(newsList, file);
				        jaxbMarshaller.marshal(newsList, System.out);
				
					} catch (JAXBException e) {
						e.printStackTrace();
						logger.writeError("nnNDataCreation"+e.getMessage() );
					}
	        }
	        else{
	        	
				try {	
				        JAXBContext jaxbContext = JAXBContext.newInstance(NewsList.class);
				        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
				        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				        NewsList newsList= new NewsList();
				        NewsItem newsItem= new NewsItem(news.getTitle(),news.getBody(),news.getAuthor(),sdf.format(cal.getTime()));			        
				        newsList.addNews(newsItem);
				        jaxbMarshaller.marshal(newsList, file);
				        jaxbMarshaller.marshal(newsList, System.out);
				        
					} catch (JAXBException e) {
						e.printStackTrace();
						logger.writeError("nnNDataCreation"+"::"+e.getMessage() );
					}
		      }
	      }	 
	      catch (Exception e) {
			e.printStackTrace();
			logger.writeError("nnNDataCreation"+"::"+e.getMessage() );
		  }
	 
		 return Response.status(200).entity("success").build();

	}
	@POST
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_XML)
	public Response updateInJSON(News news) {

		 Logger logger = new Logger(logDirectory);
		 Calendar cal = Calendar.getInstance();
	     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
       
	      try {
	    	  
	    	DocumentBuilder db = dbf.newDocumentBuilder();
	        File file = new File(datapath+news.getTitle()+news.getAuthor()+".xml");

	        if(file.exists() && !file.isDirectory()) 
	        { 

				try {
				    	Document document = db.parse(file);
				        JAXBContext jaxbContext = JAXBContext.newInstance(NewsList.class);
				        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
				        Binder<Node> binder = jaxbContext.createBinder();
				        NewsList newsList = (NewsList) binder.unmarshal(document);
				        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				        NewsItem newsItem= new NewsItem(news.getTitle(),news.getBody(),news.getAuthor(),sdf.format(cal.getTime()));			        
				        newsList.addNews(newsItem);
				        jaxbMarshaller.marshal(newsList, file);
				        jaxbMarshaller.marshal(newsList, System.out);
				
					} catch (JAXBException e) {
						e.printStackTrace();
						logger.writeError("nnNDataCreation"+e.getMessage() );
					}
	        }
	        else{
	        	
				try {	
				        JAXBContext jaxbContext = JAXBContext.newInstance(NewsList.class);
				        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
				        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				        NewsList newsList= new NewsList();
				        NewsItem newsItem= new NewsItem(news.getTitle(),news.getBody(),news.getAuthor(),sdf.format(cal.getTime()));			        
				        newsList.addNews(newsItem);
				        jaxbMarshaller.marshal(newsList, file);
				        jaxbMarshaller.marshal(newsList, System.out);
				        
					} catch (JAXBException e) {
						e.printStackTrace();
						logger.writeError("nnNDataCreation"+"::"+e.getMessage() );
					}
		      }
	      }	 
	      catch (Exception e) {
			e.printStackTrace();
			logger.writeError("nnNDataCreation"+"::"+e.getMessage() );
		  }
	 
		 return Response.status(200).entity("success").build();

	}
}
