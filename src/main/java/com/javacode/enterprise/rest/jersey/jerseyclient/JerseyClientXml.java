package com.javacode.enterprise.rest.jersey.jerseyclient;

import com.javacode.enterprise.rest.jersey.News;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class JerseyClientXml {

	public static void main(String[] args) {
		try {

			News st = new News("Assaignment5", "Assaignment for Cefalo company","turja");

			Client client = Client.create();

		
//			
			WebResource webResource = client
					.resource("http://127.0.0.1:8080/NewsServiceAssignment/rest/xmlServices/send");
//			WebResource webResource = client
//					.resource("http://127.0.0.1:8080/NewsServiceAssignment/rest/xmlServices/update");



			
			ClientResponse response = webResource.accept("application/xml")
			.post(ClientResponse.class, st);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			String output = response.getEntity(String.class);

			System.out.println("Server response .... \n");
			System.out.println(output);

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

}
