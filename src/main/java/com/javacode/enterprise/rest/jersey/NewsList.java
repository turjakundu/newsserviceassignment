package com.javacode.enterprise.rest.jersey;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;



@XmlRootElement(name = "newslist")
@XmlAccessorType (XmlAccessType.FIELD)
public class NewsList {

   @XmlElement(name = "news")
   
   protected List<NewsItem> newslist = null;

   //private OperatorRepository operatorRepository;
   public List<NewsItem> getNews() {
      return this.newslist;
   }

   public void setNews( List<NewsItem> value) {

      this.newslist = value;
   }	 
   public NewsList() {
	   newslist = new ArrayList<NewsItem>();
   }
   public void addNews( NewsItem value) {
	      this.newslist.add(value);
   }	
	
   public String  getStory() {
	   String storyDetails ="";
	   
	   NewsItem lastItem= newslist.get(newslist.size() - 1);
	   storyDetails = "Title : " + lastItem.getTitle()+"\n";
	   storyDetails = storyDetails + "Body : " + lastItem.getBody()+"\n";
	   storyDetails = storyDetails + "Auathor : "+ lastItem.getAuthor() +"\n";
	   storyDetails = storyDetails + "Date : "+ lastItem.getDate()+"\n";
	   return storyDetails;
	   
   }




}